import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { AutenticacaoProvider } from '../../providers/autenticacao.provider';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private formLogin: FormGroup;
  private loading: any;

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private autenticacao: AutenticacaoProvider
  ) {
    this.formLogin = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      senha: new FormControl('', [Validators.required]),
    });
  }

  async login() {
    let loading = this.loadingCtrl.create();
    loading.present();

    try {
      await this.autenticacao.login(this.formLogin.value.email, this.formLogin.value.senha);

      this.navCtrl.setRoot('ProtocolosPage');
    } catch(e) {
      console.log('E', e);

      this.alertCtrl.create({
        title: 'Não foi possível acessar o app, tente novamente',
        buttons: ['Ok']
      }).present();
    } finally {
      loading.dismiss();
    }
  }
}
