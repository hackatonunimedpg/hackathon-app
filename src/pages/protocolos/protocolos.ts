import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { ProtocolosProvider } from '../../providers/protocolos.provider';

@IonicPage()
@Component({
  selector: 'page-protocolos',
  templateUrl: 'protocolos.html',
})
export class ProtocolosPage {

  private erro: boolean = false;
  private data: any[];

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private navParams: NavParams,
    private protocolos: ProtocolosProvider
  ) { }

  async ionViewDidLoad() {
    this.getProtocolos();
  }

  private async getProtocolos() {
    this.erro = false;

    let loading = this.loadingCtrl.create();
    loading.present();

    try {
      this.data = await this.protocolos.getProtocolos();
    } catch(e) {
      console.log('ERROR', e);

      this.alertCtrl.create({
        title: 'Não foi possível carregar as suas solicitações de Liberação de Guia, tente novamente',
        buttons: ['Ok']
      }).present();

      this.erro = true;
    } finally {
      loading.dismiss();
    }
  }

  onClickProtocolo(protocolo: any){
    if(protocolo.status_tipo == 'EM_ANALISE') this.navCtrl.push('DetalhesProtocoloPage', { codProtocolo: protocolo.nro_protocolo });
  }
}
