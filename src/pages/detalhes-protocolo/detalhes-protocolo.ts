import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { ProtocolosProvider } from '../../providers/protocolos.provider';

@IonicPage()
@Component({
  selector: 'page-detalhes-protocolo',
  templateUrl: 'detalhes-protocolo.html',
})
export class DetalhesProtocoloPage {

  private erro: boolean = false;
  private codProtocolo: string;
  private protocolo: any;

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private navParams: NavParams,
    private protocolos: ProtocolosProvider
  ) {
    this.codProtocolo = this.navParams.get('codProtocolo');
  }

  ionViewDidLoad() {
    this.getProtocolo();
  }

  private async getProtocolo() {
    this.erro = false;

    let loading = this.loadingCtrl.create();
    loading.present();

    try {
      this.protocolo = await this.protocolos.getProtocolo(this.codProtocolo);
    } catch(e) {
      console.log('ERROR', e);

      this.alertCtrl.create({
        title: 'Não foi possível carregar os dados do protocolo, tente novamente',
        buttons: ['Ok']
      }).present();

      this.erro = true;
    } finally {
      loading.dismiss();
    }
  }

}
