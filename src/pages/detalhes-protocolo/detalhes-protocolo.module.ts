import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesProtocoloPage } from './detalhes-protocolo';

@NgModule({
  declarations: [
    DetalhesProtocoloPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesProtocoloPage),
  ],
})
export class DetalhesProtocoloPageModule {}
