import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { URL } from '../config';

@Injectable()
export class ProtocolosService {

  constructor(
    private http: Http
  ) { }

  async getProtocolos(cartao: string) {
    // let dados: any = await this.http.post(`${URL}/guias/listar`, { codBenef: cartao }).toPromise();
    let dados: any = await this.http.post(`${URL}/guias/listar`, { }).toPromise();

    let data = JSON.parse(dados._body || '[]');

    return data;
  }

  async getProtocolo(protocolo: string) {
    let dados: any = await this.http.post(`${URL}/guias/detalhes`, { codProtocolo: protocolo }).toPromise();

    console.log(dados);

    let data = JSON.parse(dados._body || '[]');

    return data;
  }

}
