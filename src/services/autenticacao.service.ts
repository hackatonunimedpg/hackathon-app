import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { URL } from '../config';

@Injectable()
export class AutenticacaoService {

  constructor(
    private http: Http
  ) { }

  async login(email: string, senha: string) {
    let dados: any = await this.http.post(`${URL}/login`, {
      email: email,
      password: senha,
      device: 1
    }).toPromise();

    let data = JSON.parse(dados._body || '[]');

    if(data.length == 0) throw 'E-mail/senha incorreto';

    return data[0];
  }

}
