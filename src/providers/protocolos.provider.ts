import { Injectable } from '@angular/core';

import { AutenticacaoProvider } from './autenticacao.provider';

import { ProtocolosService } from '../services/protocolos.service';

@Injectable()
export class ProtocolosProvider {

  constructor(
    private autenticacao: AutenticacaoProvider,
    private protocolos: ProtocolosService
  ) { }

  private setStatus(status: string) {
    switch(status) {
      case 'Em análise':
        return 'EM_ANALISE';
      case 'Parcialmente autorizado':
        return 'PARC_AUTORIZADO';
      case 'Autorizado':
        return 'AUTORIZADO';
      case 'Negado':
        return 'NEGADO';
    }
  }

  async getProtocolos() {
    // let cartao = (this.autenticacao.usuario.benefic_nro_cart || '83928470915263975');
    let cartao = '';

    let dados = await this.protocolos.getProtocolos(cartao);

    dados.map((protocolo) => {
      protocolo.status_tipo = this.setStatus(protocolo.status);

      console.log(protocolo);
    });

    return dados;
  }

  async getProtocolo(protocolo: string) {
    let dados = await this.protocolos.getProtocolo(protocolo);

    return dados;
  }

}
