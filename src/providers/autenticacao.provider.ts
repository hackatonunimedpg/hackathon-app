import { Injectable } from '@angular/core';

import { AutenticacaoService } from '../services/autenticacao.service';

@Injectable()
export class AutenticacaoProvider {

  usuario: any;

  constructor(
    private autenticacao: AutenticacaoService
  ) { }

  async login(email: string, senha: string) {
    let usuario = await this.autenticacao.login(email, senha);

    if(usuario) delete usuario.password;

    this.usuario = usuario;

    return true;
  }

}
